/**
 * It takes two arguments, name and message, and returns an object with the name, message, and date
 * @param name - The name of the user who sent the message.
 * @param message - The message that the user has sent.
 * @returns An object with the name, message, and date.
 */
const newMessage = (name, message) => {
  return {
    name,
    message,
    dates: new Date().getTime(),
  };
};

module.exports = { newMessage };
