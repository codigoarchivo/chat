class Users {
  constructor() {
    this.users = [];
  }

  /**
   * It returns the value of the users property.
   * @returns The users array.
   */
  get getUsers() {
    return this.users;
  }

  /**
   * The function takes in an id, name, and room, and pushes an object with those properties into the
   * users array
   * @param id - the socket id
   * @param name - the name of the user
   * @param room - the room that the user is in
   * @returns The users array.
   */
  addUser(id, name, room) {
    let user = { id, name, room };
    this.users.push(user);
    return this.users;
  }

  /**
   * The function takes an id as an argument, filters the users array for a user with a matching id, and
   * returns the user
   * @param id - The id of the user to get.
   * @returns The user object with the matching id.
   */
  getUser(id) {
    const user = this.users.filter((user) => user.id === id)[0];
    return user;
  }

  /**
   * It takes a room name as an argument and returns an array of users that are in that room
   * @param room - The room name
   * @returns An array of users that are in the room.
   */
  getUserByRoom(room) {
    const roomUsers = this.users.filter((user) => user.room === room);
    return roomUsers;
  }

  /**
   * It takes in an id, finds the user with that id, deletes that user from the users array, and returns
   * the deleted user
   * @param id - The id of the user to be deleted.
   * @returns The deleted user
   */
  deleteUser(id) {
    const deletedUsers = this.getUser(id);
    this.users = this.users.filter((user) => user.id != id);
    return deletedUsers;
  }
}

module.exports = {
  Users,
};
