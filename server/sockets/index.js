const { io } = require("..");
const { newMessage } = require("../utils");
const { Users } = require("../classes/users");

const users = new Users();

io.on("connection", (client) => {
  /* This is the code that is executed when a user enters the chat. */
  client.on("enterChat", (data, callback) => {
    /* Checking if the name and room are not empty. */
    if (!data.name || !data.room)
      return callback({
        error: true,
        message: "The name/room is required",
      });

    /* Joining the client to a room. */
    client.join(data.room);

    /* Adding a user to the users array. */
    users.addUser(client.id, data.name, data.room);

    /* Sending the user list to the room. */
    client.broadcast
      .to(data.room)
      .emit("userList", users.getUserByRoom(data.room));

    /* Sending a message to all the users in the room except the one who sent the message. */
    client.broadcast
      .to(data.room)
      .emit("createMesage", newMessage("Admin", `${data.name} he joined`));

    /* Sending the user list to the client. */
    callback(users.getUserByRoom(data.room));
  });

  /* This is the code that is executed when a user sends a message. */
  client.on("createMesage", (data, callback) => {
    /* Getting the user from the users array. */
    const user = users.getUser(client.id);

    /* Creating a new message with the user name and the message. */
    const message = newMessage(user.name, data.message);

    /* Sending a message to all the users in the room except the one who sent the message. */
    client.broadcast.to(user.room).emit("createMesage", message);

    /* Sending the message to the client. */
    callback(message);
  });

  /* This is the code that is executed when a user disconnects from the chat. */
  client.on("disconnect", () => {
    /* Deleting the user from the users array. */
    const deletedser = users.deleteUser(client.id);

    /* Sending a message to all the users in the room except the one who sent the message. */
    client.broadcast
      .to(deletedser.room)
      .emit("createMesage", newMessage("Admin", `${deletedser.name} he left`));

    /* Sending the user list to the room. */
    client.broadcast
      .to(deletedser.room)
      .emit("userList", users.getUserByRoom(deletedser.room));
  });

  client.on("privateMessage", (data) => {
    /* Getting the user from the users array. */
    const user = users.getUser(client.id);
    console.log(user);
    /* Sending a private message to a specific user. */
    client.broadcast
      .to(data.for)
      .emit("privateMessage", newMessage(user.name, data.message));
  });
});
