const express = require("express");
const socketIO = require("socket.io");
const http = require("http");
const path = require("path");

/* Creating an express application. */
const app = express();

/* Telling the server to use the favicon.png file as the favicon of the application. */
app.use("/favicon.png", express.static("assets/images/favicon.png"));

/* Creating a server with the app. */
let server = http.createServer(app);

/* Setting the publicPath to the public folder. */
const publicPath = path.resolve(__dirname, "../public");

/* A way to set the port of the server. */
const port = process.env.PORT || 3000;

/* Telling the server to use the publicPath folder as the static folder. */
app.use(express.static(publicPath));

/* Exporting the socketIO to the file sockets.js. */
module.exports.io = socketIO(server);

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/views/index.html");
});

app.get("/chat", (req, res) => {
  res.sendFile(__dirname + "/views/chat.html");
});

/* Importing the file sockets.js. */
require("./sockets");

/* Listening to the port 3000. */
server.listen(port, (err) => {
  if (err) throw new Error(err);
  console.log("---------------------------------");
  console.log(`Connected in the port: ${port}`);
  console.log("---------------------------------");
});
