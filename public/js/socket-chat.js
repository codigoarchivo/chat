/* Creating a connection with the server. */
const socket = io();

/* Creating a new instance of the ChatListAndSend class. */
const chat = new ChatListAndSend();

/* Destructuring the has property from the URLSearchParams object. */
const params = new URLSearchParams(window.location.search);

/* Checking if the params has a name and a room. If not, it will redirect to the index.html page. */
// if (!params.has("name") || !params.has("room")) {
//   window.location = "index.html";
//   throw new Error("The name and room are necessary");
// }

/* Creating an object with the name and room properties. */
const data = {
  name: params.get("name"),
  room: params.get("room"),
};

/* Listening to the connect event. When the connection is established, it will emit the event enterChat
with the data object. */
socket.on("connect", function () {
  console.log("Connected to the server");

  socket.emit("enterChat", data, function (resp) {
    chat.renderUsers(resp);
  });
});

/* Listening to the disconnect event. When the connection is lost, it will print a message in the
console. */
socket.on("disconnect", function () {
  console.log("We lost connection with the server");
});

/* Listening to the submit event from the form. When the event is emitted, it will prevent the
default behavior of the form. Then, it will check if the input is empty. If it is, it will return.
If not,
it will emit the createMessage event with the data object. When the event is emitted, it will call
the
callback function. The callback function will clear the input and call the renderMessages function
from
the ChatListAndSend class. */
chat.ById("formSend").addEventListener("submit", (e) => {
  e.preventDefault();

  if (!chat.ById("txtMessage").value) return;

  socket.emit(
    "createMesage",
    {
      name: params.get("name"),
      message: chat.ById("txtMessage").value,
    },

    function (message) {
      chat.ById("txtMessage").value = "";
      console.log(message);
      chat.renderMessages(message, true);
    }
  );
});

/* Listening to the createMessage event. When the event is emitted, it will call the renderMessages
function from the ChatListAndSend class. */
socket.on("createMesage", function (message) {
  console.log(message);
  chat.renderMessages(message, false);
});

/* Listening to the userList event. When the event is emitted, it will call the renderUsers
function from the ChatListAndSend class. */
socket.on("userList", function (user) {
  chat.renderUsers(user);
});

/* Listening to the privateMessage event. When the event is emitted, it will print the message in the
console. */
socket.on("privateMessage", function (message) {
  console.log("Private message:", message);
});
