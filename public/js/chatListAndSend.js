class ChatListAndSend {
  constructor() {
    /* This is a function that takes in an id parameter and returns the document.getElementById(id)
   method. */
    this.ById = (id) => document.getElementById(id);
  }

  /**
   * It takes an array of users, loops through them, and creates a list item for each user
   * @param users - The array of users to be rendered.
   */
  renderUsers(users) {
    /* Checking if the users array has an error property. If it does, it returns. */
    if (users.error) return;

    let html = "";
    users.map(
      (user) =>
        (html += `
        <li>
          <a href="#">
            <img src="assets/images/icon.png" alt="user-img" class="img-circle"> 
            <span>${user.name} <small class="text-success">online</small></span>
          </a>
        </li>
      `)
    );
    this.ById("divUsers").innerHTML = html;
  }

  /**
   * It creates a new HTML element, adds some classes to it, and then adds it to the DOM
   * @param message - The message object that contains the message, name, and date.
   * @param valid - This is a boolean value that determines whether the message is from the user or not.
   */
  renderMessages(message, valid) {
    const date = new Date(message.dates);
    let adminClass = "info";

    if (message.name === "Admin") adminClass = "danger";

    const li = document.createElement("li");
    if (valid) {
      li.classList.add("animated");
      li.classList.add("fadeIn");
      li.classList.add("reverse");
      li.innerHTML += `
      <div class="chat-content-img">
        <div class="chat-content">
          <h5>${message.name}</h5>
          <div class="bg-light-inverse"><p>${message.message}</p></div>
        </div>
        <div class="chat-img">
          <img src="assets/images/icon.png" alt="user" />
        </div>
      </div>
      <div class="chat-time">${date.getHours() + ":" + date.getMinutes()}</div>
     `;
    } else {
      li.classList.add("animated");
      li.classList.add("fadeIn");
      li.classList.add("odd");

      li.innerHTML += `
      <div class="chat-content-img">
        <div class="chat-img">
            <img src="assets/images/icon.png" alt="user" />
        </div>
        <div class="chat-content">
           <h5>${message.name}</h5>
            <div class="bg-light-${adminClass}">
                <p>${message.message}</p>
            </div>
         </div> 
      </div>
      <div class="chat-time">${date.getHours() + ":" + date.getMinutes()}</div>
     `;
    }

    this.ById("divChatbox").append(li);
  }
}
